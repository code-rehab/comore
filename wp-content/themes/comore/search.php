<?php
/*
Template Name: Search Page
*/
?>

<?php
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<?php if($feat_image){ ?>
  <header id='banner' style="background-image:url(<?php echo $feat_image;  ?>);"></header>
  <?php }else{ ?>
    <header id='banner' style="background-image:url('<?php bloginfo('wpurl') ?>/wp-content/uploads/2015/08/slide1.jpg');"></header>
    <?php } ?>


    <section id="content">

      <section id="breadcrumbs">
        <section class="pagewrap">

          <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
            yoast_breadcrumb();
          }?>

        </section>

      </section>

      <section  class="pagewrap">
        <?php
        global $query_string;

        $query_args = explode("&", $query_string);
        $search_query = array();

        if( strlen($query_string) > 0 ) {
          foreach($query_args as $key => $string) {
            $query_split = explode("=", $string);
            $search_query[$query_split[0]] = urldecode($query_split[1]);
          } // foreach
        } //if

        $search = new WP_Query($search_query);
        ?>

        <article>
          <h1>Zoeken</h1>

          <!--				<?php get_search_form(); ?>-->

          <section id="search" class="full">
            <form action="<?php echo home_url( '/' ); ?>" method="get">
              <label for="search-field">Zoeken naar: </label>
              <input value="<?php echo get_search_query() ?>" type="text" name="s" placeholder="Uw zoekterm" id="search-field" /><button type="submit"><i class="fa fa-search"></i></button>
            </form>
          </section>

          <?php if (isset($search_query['s']) && $search_query['s'] != '' && $search->have_posts()) {
          				while ( $search->have_posts() ) : $search->the_post(); ?>

            <section class='post-summary'>
              <h2><?php the_title(); ?></h2>
              <?php the_excerpt(); ?>
              <a class='button medium'href='<?= the_permalink() ?>'>Lees verder</a>
            </section>

            <hr>

            <?php endwhile; ?>

            <?php wp_reset_query(); ?>
            <?php wp_reset_postdata(); ?>

            <?php } else {
              echo '<h2>Er zijn helaas geen resultaten gevonden</h2>';
            }?>

            <section class="pagination">
              <?php echo paginate_links(  ); ?>
            </section>
          </article>
        </section>


      </section>



      <?php get_footer(); ?>
