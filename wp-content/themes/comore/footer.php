<footer>
	<section class="pagewrap">


		<section class="sort">

		<section class="col quick-nav">
			<h2>Snel naar</h2>
			<?php wp_nav_menu( array('theme_location' => 'bottom-menu', 'container' => '', 'items_wrap' => '<ul class="arrows">%3$s</ul>') ); ?>
		</section>

		<section class="col contact">
			<h2>Contact</h2>
			<p><span class="bold">Kiwa CMR</span><br>
			Postbus 2703<br>
			3430 GN Nieuwegein<br>
			<a href="mailto:info@co-more.nl" class="bold"><i class="fa fa-envelope"></i>info@co-more.nl</a>
			<a href="tel:088-9984300" class="bold"><i class="fa fa-phone"></i>088 - 998 43 00</a>
			</p>
		</section>

		</section>

		<section class="col follow-us">
			<h2>Volg ons</h2>
			<a href="https://www.facebook.com/comorezeist" target="_blank" class="icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="https://nl.linkedin.com/company/comore" target="_blank" class="icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			<ul>
				<li><a href="<?php bloginfo('wpurl')?>/disclaimer">Disclaimer</a></li>
				<li><a href="<?php bloginfo('wpurl')?>/copyright">Copyright</a></li>
				<li><a href="<?php bloginfo('wpurl')?>/pricacy-statement">Privacy Statement</a></li>
			</ul>
		</section>


	</section>
</footer>

<script src="<?php bloginfo('template_url'); ?>/assets/js/external/jquery.touchSwipe.min.js"></script>

<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/assets/js/slider.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/site.js"></script>

</body>
</html>
