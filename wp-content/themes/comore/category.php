<?php
get_header(); 
$catname =  get_cat_name( $category_id );

if (is_category( )) {
  $cat = get_query_var('cat');
  $yourcat = get_category ($cat);
 }
?>

<header id='slider'>
	<?php echo do_shortcode('[rehabslider width="auto" id="slider_home" class="homepage-slider" post_type="slider" custom_tax="tax_slider" show_bullets="1" category_name="homepagina-slider"]'); ?>
</header>

<section id="breadcrumbs">
	<section class="pagewrap">
		
		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
            yoast_breadcrumb();
    }?>   
	
	</section>

</section>

<section id="page-content">
	<section class="pagewrap">	
		<article>
			<h1><?php echo $catname; ?></h1>
			<?php echo category_description( $category_id ); ?>
			</article>
	   <section id="subpage-links">
			<?php

				 $page_links = get_posts(array(
    'category_name' => $yourcat->slug,
	'post_parent' => 0,
    'post_type' => 'page',
    'post_status' => 'publish',
    'orderby' => 'menu_order',
	'order' => 'ASC'
  ));
echo '<ul>';
  foreach($page_links as $link){
	  echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a>';
	 //print_r($link);
	  $subpage_links = get_posts(array(
			'post_parent' => $link->ID,
			'post_type' => 'page',
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		  ));
	  
	  // check subitems
	  
	  
	  echo '<ul>';
	    foreach($subpage_links as $link){
	  echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
		}
	  echo '</ul></li>';
  }
 
echo '</ul>';
?>
		</section>
			
		
		<aside>
			
		</aside>
        
	</section>
</section>

<?php get_footer(); ?>