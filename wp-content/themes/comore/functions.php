<?php

//display errors
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

//INCLUDES
//=======================================================================

//nothing yet
require_once "core/functions/mobile-detect.php";
require_once "core/functions/featured-image.php";
require_once "core/functions/default-gallery.php";

require_once "core/metaboxes/slide.metabox.php";
require_once "core/metaboxes/slider-select/metabox.php";

require_once "core/post-types/sliders.ptype.php";
require_once "core/post-types/FAQ.ptype.php";

require_once "core/shortcodes/cr-post-slider.shortcode.php";
require_once "core/shortcodes/boekingsform.shortcode.php";
require_once "core/shortcodes/show-posts.shortcode.php";

// function font_admin_init() {
//    add_editor_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome-4.4.0/css/font-awesome.min.css');
// }
// add_action('admin_init', 'font_admin_init');


//SETTINGS, ACTIONS, FILTERS
//=======================================================================

//settings
add_theme_support(
    'post-thumbnails', array(
        'post',
        'events',
        'page',
        'partners',
        'team',
        'company',
        'slider'
    )
);


function myplugin_settings() {
// Add tag metabox to page
register_taxonomy_for_object_type('post_tag', 'page');
// Add category metabox to page
register_taxonomy_for_object_type('category', 'page');
}
 // Add to the admin_init hook of your theme functions.php file
add_action( 'init', 'myplugin_settings' );

// add_image_size('single', 1200, 300, true);

//actions
add_action( 'after_setup_theme', 'register_default_menus' );
add_action( 'widgets_init', 'register_default_sidebars' );
add_action( 'admin_init', 'set_custom_editor_stylesheet' );
add_action( 'init', 'init_custom_sidebars_post_types' );

//filters
add_filter('mce_buttons_2', 'add_editor_styleselect_btn');
add_filter( 'tiny_mce_before_init', 'setup_custom_editor_settings' );
add_filter('wp_mail_from', 'set_mail_from');
add_filter('wp_mail_from_name', 'set_mail_from_name');

// functions
//=======================================================================

//action: widgets_init
function register_default_sidebars() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'default-sidebar' ),
        'id'            => 'page-sidebar',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="title">',
        'after_title'   => '</h2>',
    ));

    register_sidebar( array(
        'name'          => __( 'Footer', 'footer' ),
        'id'            => 'footer',
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title">',
        'after_title'   => '</h3>',
    ));
}

function init_custom_sidebars_post_types() {
    add_post_type_support( 'post', 'simple-page-sidebars' );
    add_post_type_support( 'faq', 'simple-page-sidebars' );
}

//action: after_setup_theme
//register menus
function register_default_menus() {
    register_nav_menu( 'bottom-menu', 'bottom-menu' );
	register_nav_menu( 'main-menu', 'Main-menu' );
    register_nav_menu( 'top-menu', 'top-menu' );
	register_nav_menu( 'wie-is-comore-menu', 'wie-is-comore-menu' );
	register_nav_menu( 'comore-voor-menu', 'comore-voor-menu' );
}

function set_custom_editor_stylesheet() {
    add_editor_style( 'assets/css/wp-editor-style.css' );
    add_editor_style( 'assets/css/font-awesome-4.4.0/css/font-awesome.min.css' );
}

// filter: tiny_mce_before_init
function setEditorColors( $init ) {

}

// filter: wp_mail_from
function set_mail_from($old) {
    return get_option('admin_email');
}

// filter: wp_mail_from_name
function set_mail_from_name($old) {
    return get_option('blogname');
}

//filter:mce_buttons_2
function add_editor_styleselect_btn( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

//filter:tiny_mce_before_init
function setup_custom_editor_settings( $init_array ) {

    // Define the style_formats array
    $style_formats = array(
      array(
          'title' => 'Primary Button',
          'inline' => 'button',
          'classes' => '',
          'wrapper' => false,
      ),

        array(
            'title' => 'Secondary Button',
            'inline' => 'button',
            'classes' => 'bg-color-2',
            'wrapper' => false,
        ),

        array(
            'title' => 'Gray Button',
            'inline' => 'button',
            'classes' => 'bg-color-3',
            'wrapper' => false,
        ),

        array(
            'title' => 'Button-full-width',
            'inline' => 'span',
            'classes' => 'button full-width',
            'wrapper' => false,
        ),

        array(
            'title' => 'PDF button',
            'inline' => 'span',
            'classes' => 'button icon-pdf',
            'wrapper' => false,
        ),
    );

    //change default colors
    $default_colours = '[
	    "000000", "Black",
	    "ffffff", "White",
	    "ababab", "Gray",
	    "FA950F", "Orange1",
	    "DF6421", "Orange2",
	    "096af3", "Blue1",
	    "389cff", "Blue2",
	]';

    $init_array['textcolor_map'] = $default_colours;
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}
