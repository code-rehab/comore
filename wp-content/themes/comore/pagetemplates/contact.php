<?php

/*
Template Name: Contact
*/

get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<?php if($feat_image){ ?>
	<header id='banner' style="background-image:url(<?php echo $feat_image;  ?>);"></header>
<?php }else{ ?>
	<header id='banner' style="background-image:url('<?php bloginfo('wpurl') ?>/wp-content/uploads/2015/08/slide1.jpg');"></header>
<?php } ?>

<section id="breadcrumbs">
	<section class="pagewrap">

		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
            yoast_breadcrumb();
    }?>

	</section>

</section>


<section id="page-content">
	<section class="pagewrap">
		<?php while ( have_posts() ) : the_post(); ?>
        <article>
            <?php the_content(); ?>

        </article>
<?php endwhile; ?>
	</section>
</section>

<section class="pagewrap">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2454.8951656799622!2d5.063242351517129!3d52.027007480299226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c664f72973afbd%3A0xf619d089c909b6d!2sNevelgaarde+20%2C+3436+ZZ+Nieuwegein!5e0!3m2!1snl!2snl!4v1489056082426" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>


<?php get_footer(); ?>
