<?php

/*
Template Name: Landing
*/

get_header('landing'); ?>

<header id='banner'>
	
</header>

<section id="quote">
	<section class="pagewrap">
		<h2>Varkenshouders kunnen zich reeds aanmelden voor <br>deelname aan het <strong>Varken van Morgen</strong></h2>
		<button class="btn call-to-action-btn">Direct aanmelden <i class="fa fa-angle-right"></i></button>
	</section>
</section>


<section id="page-content">
	<section class="pagewrap">

		<article>
			<?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
<?php endwhile; ?>
		</article>
		
		
		<section id="call-to-action">
			<h2>Meld u zich nu direct aan voor deelname aan <strong>Varken van Morgen.</strong></h2>
			<button class="btn call-to-action-btn">Direct aanmelden <i class="fa fa-angle-right"></i></button>
			<h2>of bel <strong><a href="tel:030 694 18 80">030 694 18 80</a></strong> voor meer informatie </h2>
		</section>
		
		<article>
			<p>Als de aanmelding bij <a href="#">VERIN</a> binnen is ontvangen deelnemers een informatiepakket met daarin onder andere de normen die het programma voorschrijft. De organisatie <a href="#">CBD</a> neemt contact op voor een bezoek waarbij de normen worden gecontroleerd op het varkensbedrijf. Op basis van die rapportage zal <a href="#">VERIN</a> beoordelen of een bedrijf gecertificeerd kan worden.</p>
		</article>
		
	</section>
</section>

<?php get_footer(); ?>