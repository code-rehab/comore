<?php

/*
Template Name: Homepage
*/

get_header(); ?>

<header id='slider'>
	<?php echo do_shortcode('[rehabslider width="auto" id="slider_home" class="homepage-slider" post_type="slider" custom_tax="tax_slider" show_bullets="1" category_name="homepagina-slider"]'); ?>
</header>



<section id="blocks">

	<section class="pagewrap">

		<section class="wrapper">
			<section class="block">
				<figure style="background-image:url('wp-content/uploads/2015/09/1.jpg');"></figure>

				<h2>Wie is CoMore</h2>
				<?php wp_nav_menu( array('menu' => 'Main', 'theme_location' => 'wie-is-comore-menu', 'items_wrap' => '<ul class="arrows">%3$s</ul>' )); ?>
			</section>
		</section>

		<section class="wrapper">
			<section class="block">
				<figure style="background-image:url('wp-content/uploads/2015/09/2.jpg');"></figure>
				<h2>CoMore voor:</h2>
				<?php wp_nav_menu( array('menu' => 'Main', 'theme_location' => 'comore-voor-menu', 'items_wrap' => '<ul class="arrows">%3$s</ul>' )); ?>
			</section>
		</section>

		<section class="wrapper">
			<section class="block">
				<figure style="background-image:url('wp-content/uploads/2015/09/3.jpg');"></figure>
				<h2>Actualiteiten</h2>
				<ul class="arrows">
					<?php
					$args = array(
						'posts_per_page' => 3,
						'cat'   => 'actualiteiten'
					);
					$query = new WP_Query($args);?>

					<?php while ($query->have_posts() ) : $query->the_post(); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				</ul>

			</section>
		</section>

	</section>

</section>

<section id="page-content">
	<section class="pagewrap">

		<article>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</article>
	</section>
</section>

<?php get_footer(); ?>
