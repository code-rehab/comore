<!doctype html>

<html>
	<head>
		<meta charset="utf-8">

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="author" content="code.rehab" />
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">

		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/assets/images/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/assets/images/favicon-16x16.png" sizes="16x16" />

		<title><?php wp_title(); ?></title>
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link id='mainstyle' rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/site.css"  />
		<link id='mainstyle' rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome-4.4.0/css/font-awesome.min.css"  />
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>



	</head>
	<body <?php body_class() ?> >

		<header id='top'>
			<section class='pagewrap'>
				<a href='<?= bloginfo('url') ?>' id='logo'><img src='<?php bloginfo( 'template_url' ); ?>/assets/images/logo.jpg' height='115'/></a>
				<nav id="top">
					<section id="language-selector">
						<i class="fa fa-angle-down"></i> <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/nl-flag.jpg"/>
					</section>
					

					<ul><li><a href="<?php bloginfo('wordpress_url') ?>/home">Naar de website</a></li></ul>
				</nav> 
			</section>
		</header>

		

		<section id="search">
			<form method="get">
				<input type="text" name="s" placeholder="Uw zoekterm landing" >
				<button type="submit" class="btn search-icon"><i class="fa fa-search"></i></button>
			</form>			
		</section>


