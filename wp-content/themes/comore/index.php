<?php get_header(); ?>


<header id='slider'>
	<?php echo do_shortcode('[rehabslider width="auto" id="slider_home" class="homepage-slider" post_type="slider" custom_tax="tax_slider" show_bullets="1" category_name="homepagina-slider"]'); ?>
</header>

<section id="breadcrumbs">
	<section class="pagewrap">
		
		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
            yoast_breadcrumb();
    }?>   
	
	</section>

</section>

<section id="page-content">
	<section class="pagewrap">
		<?php while ( have_posts() ) : the_post(); ?>
        <article>
			<h2><?php the_title(); ?></h2>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Lees verder</a>
        </article>
<?php endwhile; ?>
	</section>
	
	
</section>


<?php get_footer(); ?>