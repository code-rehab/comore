<?php
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<?php if($feat_image){ ?>
	<header id='banner' style="background-image:url(<?php echo $feat_image;  ?>);"></header>
<?php }else{ ?>
	<header id='banner' style="background-image:url('<?php bloginfo('wpurl') ?>/wp-content/uploads/2015/08/slide1.jpg');"></header>
<?php } ?>

<section id="breadcrumbs">
	<section class="pagewrap">
		
		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
            yoast_breadcrumb();
    }?>   
	
	</section>

</section>

<section id="page-content">
	<section class="pagewrap">
		<?php while ( have_posts() ) : the_post(); ?>
        <article>
            <h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
        </article>
<?php endwhile; ?>
	</section>
</section>

<?php get_footer(); ?>

<!-- 



<section id="subpage-links">
            <?php
                if($post->post_parent)
                    $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
                else
                    $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
                if ($children) { ?>
                <ul>
                    <?php echo $children; ?>
                </ul>
            <?php } ?>
            </section>







-->