<?php 
	function has_featured_image($postID=null){
		if (!$postID) {
			global $post;
			$postID = $post->ID;
		}
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $postID), 'tumbnail' ); 
		if ($image[0]) return true;
		else return false;
	}
	
	function get_featured_image($type='url', $size='full', $postID=-1) {
		if ($postID == -1) {
			global $post;
			$postID = $post->ID;
		}
		
		switch ($type) {
			case 'url':
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $postID), $size ); 
				return $image[0];
				break;
			case 'style':
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $postID), $size ); 
				if (!$image[0])
					return ""; 
				return "style='background-image: url(" . $image[0] . ")'"; 
				break;
			default: 
				return "";
				break;
		}
	}
?>