<?php 

add_action( 'add_meta_boxes', 'slide_properties_meta_box_add' );
add_action( 'save_post', 'slide_properties_meta_box_save' );

function slide_properties_meta_box_add()
{
	add_meta_box( 'slide_properties-menu-select', 'Slide options', 'slide_properties_menu_box', 'slider', 'normal', 'high' );
}

function slide_properties_menu_box( $post )
{
	$values = get_post_custom( $post->ID );

	$has_labels = isset( $values['show_slide_labels'] ) ? esc_attr( $values['show_slide_labels'][0] ) : "";
	$has_description = isset( $values['show_slide_description'] ) ? esc_attr( $values['show_slide_description'][0] ) : "";

	$heading1 = isset( $values['slide_heading1'] ) ? esc_attr( $values['slide_heading1'][0] ) : "";
	$heading2 = isset( $values['slide_heading2'] ) ? esc_attr( $values['slide_heading2'][0] ) : "";
	$url =  esc_attr( $values['post_button_url'][0] );
?>
<p>
	<input type="checkbox" name="show_slide_description" id="show_slide_description" <?php if ($has_description) echo 'checked'?>/><label for="show_slide_description">show description</label>
</p>
<p>
	<input type="checkbox" name="show_slide_labels" id="show_slide_labels" <?php if ($has_labels) echo 'checked'?>/><label for="show_slide_labels">show labels</label>
</p>

<div id='labels_props'>
	<p>
		<label for="slide_heading1">Heading 1</label><br />
		<input type="text" name="slide_heading1" id="slide_heading1" value="<?php echo $heading1; ?>" />
	</p>
	<p>
		<label for="slide_heading2">Heading 2</label><br />
		<input type="text" name="slide_heading2" id="slide_heading2" value="<?php echo $heading2; ?>" />
	</p>
	<p>
		<label for="">Url</label><br/>
		<select name="url">
			<?php $args = array(
		'sort_order' => 'asc',
		'sort_column' => 'post_title',
		'hierarchical' => 1,
		'exclude' => '',
		'include' => '',
		'meta_key' => '',
		'meta_value' => '',
		'authors' => '',
		'child_of' => 0,
		'parent' => -1,
		'exclude_tree' => '',
		'number' => '',
		'offset' => 0,
		'post_type' => 'page',
		'post_status' => 'publish'
	); 
	$pages = get_pages($args); 

	foreach($pages as $page){
		if($url == $page->ID)
			echo '<option selected value="'.$page->ID.'">'.$page->post_title.'</option>';
		else
			echo '<option value="'.$page->ID.'">'.$page->post_title.'</option>';
	}
			?>
		</select>
	</p>

</div>

<script>

	if (!jQuery('#show_slide_labels').prop('checked')) jQuery('#labels_props').hide();
	jQuery('#show_slide_labels').change(function(e){
		jQuery('#labels_props').toggle();
	});

</script>
<?php
}

function slide_properties_meta_box_save( $post_id )
{

	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;

	$has_labels = isset( $_POST['show_slide_labels'] )  ? 1 : 0;
	update_post_meta( $post_id, 'show_slide_labels', $has_labels );
	
	// url for button
	$url = $_POST["url"];
	update_post_meta( $post_id, 'post_button_url', $url );

	$has_description = isset( $_POST['show_slide_description'] ) ? 1 : 0;
	update_post_meta( $post_id, 'show_slide_description', $has_description );

	if( isset( $_POST['slide_heading1'] ) )
		update_post_meta( $post_id, 'slide_heading1', $_POST['slide_heading1']);

	if( isset( $_POST['slide_heading2'] ) )
		update_post_meta( $post_id, 'slide_heading2', $_POST['slide_heading2']);
}

?>