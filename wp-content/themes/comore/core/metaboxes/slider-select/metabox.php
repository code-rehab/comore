<?php 

add_action( 'add_meta_boxes', 'slider_select_meta_box_add' );
add_action( 'save_post', 'slider_select_meta_box_save' );

function slider_select_meta_box_add()
{
    add_meta_box( 'slider_select', 'Slider', 'slider_select_menu_box', 'page', 'normal', 'high' );
}

function slider_select_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
   
    $slider_value = isset( $values['slider'] ) ? esc_attr( $values['slider'][0] ) : "0";
    
    
    include 'view.php';
}

function slider_select_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;
    
    if( isset( $_POST['slider_value'] ) ) {
        
        $slider_value = $_POST['slider_value'];
       
        
        update_post_meta( $post_id, 'slider', $slider_value);
     
    
    }
    
}

?>