<?php
 
add_action( 'init', 'create_faq_post_type' );
function create_faq_post_type() {
	
	register_post_type( 'faq',
		array(
			'labels' => array(
				'name' => __( 'FAQ' ),
				'singular_name' => __( 'FAQ' ),
				'parent_item_colon' => '',
				'add_new' => 'Nieuw item'
			),
			'hierarchical' => true,
			'show_in_nav_menus' => true,
			'map_meta_cap' => true,
			'public' => true,
			'has_archive' => true,
			'rewrite' => array( 'slug' => 'meest-gestelde-vragen','with_front' => FALSE),
			'supports' => array('title','editor','thumbnail','excerpt','comments', 'page-attributes'),
		)
	);
}
?>