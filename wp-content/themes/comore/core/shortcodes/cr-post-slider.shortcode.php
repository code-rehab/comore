<?php
function add_cr_post_slider_func($atts) {
    extract(shortcode_atts(array(
        'id' => '',
        'class' => '',
        'title' => '',
        'category_name' => '',
        'post_type' => 'post',
        'custom_tax' => '',
        'width' => 'auto',
        'height' => 'auto',
        'count' => '-1',
        'orderby' => 'ID',
        'order' => 'DESC',
        'slide_count' => '5',
        'show_bullets' => 0,
        'show_arrows' => 1,
        'is_contentslider' => 0,
        'show_post_title' => 0,
        'show_post_excerpt' => 0,
        'show_more_button' => 0,
        'show_more_button_text' => 'Lees meer',

        'no_slide_labels' => 0,
        'no_slide_description' => 0
    ), $atts));

    global $device;

    $args = array(
        'posts_per_page' => $slide_count,
        'post_type' => "$post_type",
        'orderby' => $orderby,
        'order' => '$order',
    );

    if ($custom_tax && $category_name) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => "$custom_tax",
                'field' => 'slug',
                'terms' => "$category_name"
            )
        );
    }else if ($category_name){
        $args['category_name'] = "$category_name";
    }

    $query = new WP_Query( $args );

    $output = "<section id='$id' class='$class' ><section class='sliderContainer'>";

    while ( $query->have_posts() ) : $query->the_post();

    $has_description = get_post_meta( get_the_ID(), 'show_slide_description', true );
    $has_labels = get_post_meta( get_the_ID(), 'show_slide_labels', true );

	$url = get_post_meta( get_the_ID(), 'post_button_url', true );

	// die($url);

    $header1 = get_post_meta( get_the_ID(), 'slide_heading1', true );
    $header2 = get_post_meta( get_the_ID(), 'slide_heading2', true );

    $output .= "<figure class='sliderElem'>";
    $output .= "<section class='imageholder'>";

    if(!$is_contentslider){
        if (!$device->isMobile())
            $output .= "<img class='slideimage' src='" . get_featured_image('url', 'full') . "' />";
        else if ($device->isTablet())
            $output .= "<img class='slideimage' src='" . get_featured_image('url', 'large') . "' />";
        else if ($device->isMobile() && !$device->isTablet())
            $output .= "<img class='slideimage' src='" . get_featured_image('url', 'medium') . "' />";
        $output .= "</section>";
    }

    //if show_labels
    if ($has_labels && !$no_slide_labels):
    $output .= "<section class='labels'>";
	$output .= '<section id="quote">';
    $output .= "<section class='inner pagewrap'>";

   // $output .= "<h1>" . $header1 . "</h1><br />";
    $output .= "<h2>" . get_the_content()  . "</h2>";
    $output .= '<a href="'.get_permalink($url).'"><button class="btn call-to-action-btn">Meer informatie</button></a>';
    $output .= "</section>";
    $output .= "</section>";
	 $output .= "</section>";
    endif;
    //----

    if ($is_contentslider) {
        $output .= "<section class='slide-content'>";
        $output .= "<section class='inner'>";
        $output .= "<h2 class='title'>" . get_the_title() . "</h2>";
        $output .= "<p class='content'>" . get_the_excerpt() . "</p>";
        if ($show_more_button) $output .= "<a href='" . get_the_permalink() . "' class='button read-more'>" . $show_more_button_text . "</a>";
        $output .= "</section>";
        $output .= "</section>";
    }

    // if (!$show_more_button) $output .= "<a class='overlaybutton' href='" . get_permalink(get_the_ID()) . "'></a>";

    //if show_excerpt
    if ($has_description):
    $output .= "<section class='description'>";
    $output .= "<section class='inner'>";
    $output .= "<h2 class='title'>" . get_the_title() . "</h2>";
    $output .= "<p class='content'>" . get_the_excerpt() . "</p>";

    $output .= "</section>";
    $output .= "</section>";
    endif;
    //----

    $output .= "</figure>";

    endwhile;
    $output .= "</section>";
    if($title != '')$output .= "<h2 class='slider_title'>$title</h2>";
    if($show_arrows) $output .= "<section class='buttons'><section class='btn-prev'></section><section class='btn-next'></section></section>";
    if($show_bullets) $output .=  "<section class='pagewrap'><div class='bullets'></div></section>";
    $output .= "</section>";

   $output .= "<script>
		var $id

		jQuery(document).ready(function(){
			 $id = new AllSlider('#$id',  '$width' , '$height', {animType:'slide', animVars:{
				direction:'left',
				descriptionAnim:'',
				speed:7000,
				animSpeed:900
			}});

            $id.onResizeHandler();
		});
	</script>";
    wp_reset_query();
    return $output;
}

add_shortcode('rehabslider', 'add_cr_post_slider_func');
