<?php
function add_boekingsform_shorcode_func($atts) {
    extract(shortcode_atts(array(
        'id' => '',
        'title' => '',
        'button_text' => 'Nu boeken',
        'typecode' => '',
    ), $atts));

    $typeboekingcode = get_post_meta( get_the_ID(), 'typeboekingcode', true );
    $objboekingcode = get_post_meta( get_the_ID(), 'objboekingcode', true );
    if($typecode != '') $typeboekingcode = $typecode;
    if($typeboekingcode == '') $typeboekingcode = '1003';

    $output = "<section id='$id' class='boekingsform'>";
    $output .= "<form method='post' name='onlineboeken' action='http://boeken.dewitteberg.nl/witteberg/start.lp' target='_blank'>";
    if ($typeboekingcode) $output .= "<input type='hidden' name='objectsoort' value='$typeboekingcode'>";
    $output .= "<input type='hidden' name='lng' value='nl'>";

    if($title) $output .= "<h2>$title</h2>";
    if(!$objboekingcode) {
        $output .= "<select class='field_accommodatie' name='objecttype' size='1'>";
        $output .= "<option value='' selected=''>Kies een accommodatie...</option>";
        $output .= "<option value='1002'>Toeristische plaats</option>";
        $output .= "<option value='1012'>Toeristische Comfort plaats</option>";
        $output .= "</select>";
    }else {
        $output .= "<input type='hidden' name='objecttype' value='$objboekingcode'>";
    }

    $output .= "<input type='text'  name='arrival' id='arrival' class='datepicker' size='29' placeholder='Aankomstdatum'>";

    $output .= "<input type='text'  name='departure' id='departure' class='datepicker' size='29' placeholder='vertrekdatum'>";

    /*
    $output .= "<select class='field_nights' name='nights' size='1'>";
    $output .= "<option value='' selected=''>Aantal nachten...</option>";
    for($i = 1; $i <= 30; $i++) {
        $output .="<option value='$i'>$i</option>";
    }
    $output .= "</select>";*/

    $output .= "<input type='submit' class='submit' value='$button_text'>";
    $output .= "</form>";
    $output .= "</section>";

    return $output;
}

add_shortcode('boekingsformulier', 'add_boekingsform_shorcode_func');