
Goede morgen,

Naar aanleiding van de advertentie op de UWV site waar u een Medewerker 
personeel en organisatie zoekt, zou ik graag willen reageren.

Uw bedrijf spreekt mij aan omdat ik op zoek bent naar een eerlijk en open werkgever, en 
na de advertentie gelezen te hebben krijg ik het gevoel dat dit ook zo is.

Ruim 14 jaar heb ik ervaring als administratief medewerker personeel 
en organisatie en ben over het algemeen bekend met bijna alle voorkomende werkzaamheden.
Ik heb geen 5 jaar ervaring in uw genoemde functie maar ben bereid dit te leren.
 Tevens ben gemotiveerd en 
kan zowel zelfstandig als in teamverband werken.

Graag zou ik in een persoonlijk gesprek u willen vertellen wat ik als medewerker personeel en organisatie 
voor je kan betekenen.

Met vriendelijke groet,
Coby Graafland 