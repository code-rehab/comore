						Curiculum Vitae

Personalia

Naam				Hans Reesink
Adres				Bezelhorstweg 25
Postcode en Woonplaats	7009 KK  Doetnchem
Telefoon				06-51004096
E-mail				hans.reesink@hetnet.nl
Geboortedatum			19 december 1971
Burgerlijke staat			alleenstaand
Rijbewijs				B + C + BE, BE1+ CE+ CE1 
					met code 95 t/m 09/2021
Chauffeurskaart			Bestuurderskaart dig. tachograaf
BCT chauffeurskaart		Taxi-chauffeurskaart
				

Opleidingen

2004					Chauffeursdiploma CCV-B
					(eigen- Beroepsgoederenvervoer                                     
					over de weg)                                                                                       
2003					Taxi-chauffeursdiploma
1990 - 1992			Pretoria College
					(Nasionale Senior Sertifikaat)	
1989					Hoerlandbouskool Wagpos Z.A
					(vergelijkbaar met MBO diploma)
1983-1984				Laerskool Wonderboom-Suid
					(vergelijkbaar met basisschool en
					2 jaar MAVO)

Cursussen

2016					code 95 cursussen
2015					code 95 cursussen					
2013					Levensreddendhandelen-certificaat
2006					Sociale Vaardigheden certificaat
2011					Heftruck certifiaat

Werkervaringen

03/2015 -				Vrachtwagenchauffeur (bakwagen +                      
					 trekker oplegger) 


01/2013 - 2015			Taxi-chauffeur regiotaxi Achterhoek                                                                                             

2011 - 2012			Eigengoederen vervoer (bakwagen +
					trekker - oplegger)
					

2007-2011				Tempo-Team Doetinchem
					
werkzaamheden			vervoer banket naar winkels
					vervoer grondstoffen, vriesgoederen
					verpakkingsmateriaal

2000-2006				Taxi-Trip Doetinchem
					Taxi-chauffeur
werkzaamheden			enkel- groeps- school- zieken
		 			en aangepaste-personenvervoer

1999-2000				Forte Heritage Hotel (Engeland)
					Night-Manager
werkzaamheden			voorbereiding conferenties seminaren
					volle verantwoording nacht
					(23h00-08h00)
					bediening breakfast  lunch en dinner
					gedurende functies bediening

1997-1999				Elite Book Consultant's
					Mede-eigenaar
werkzaamheden			vertegenwoordiger na-schoolse
					studieinstellingen
					
1996-1998				Protea Boekwinkel
					Verkoopassistent
werkzaamheden			marketing
					vertegenwoordiger studieinstellingen
					verkoop academiese boeken

1995-1996				Randstad Doetinchem
					Uitzendkracht
werkzaamheden			verschillende functies bij bedrijven

1990-1995				van Schaik Boekhandel
					Verkoopassistent
werkzaamheden			verkoop
					vertegenwoordiger
					voorraadbeheer
					bedrijfsleider

Competenties

					accuraat
					zelfstandig
					teamverband
					flexibel
					secuur
					leergierig
					stressbestendig

Referenties op aanvraag